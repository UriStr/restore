export default class BookstoreService {

  data = [
    {
      id: 1,
      title: 'Production-ready Microservices',
      author: 'Susan J. Fowler',
      price: 32,
      coverImage: 'https://upload.wikimedia.org/wikipedia/ru/5/51/%D0%A1%D0%BE%D0%B1%D0%B0%D1%87%D1%8C%D0%B5_%D1%81%D0%B5%D1%80%D0%B4%D1%86%D0%B5%28%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0%29.jpg'
    },
    {
      id: 2,
      title: 'Release It!',
      author: 'Michael T. Nygard',
      price: 45,
      coverImage: 'https://chubarov.if.ua/images/book_design_2.jpg?crc=502489758'
    }
  ];

  getBooks() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(this.data);
        //reject(new Error('Smt bad happened'));
      }, 700);
    });
  }

};
