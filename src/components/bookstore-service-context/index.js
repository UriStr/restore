import {
    BookstoreServiceConsumer,
    BookstoreServiceProvider
} from './bookstore-context-service';

export {
    BookstoreServiceProvider,
    BookstoreServiceConsumer
};